from datetime import datetime
import serial
import traceback


class EbaraTurboRS232:

    terminator = '\r'
    defaultBaud = 19200
    defaultTimeout = .1
    defaultPortID = '/dev/tty_EbaraTurbo'
    
    # Defines here
    errorcode_no_command = ['#00',0,'No Command']
    errorcode_bad_parameter = ['#01' ,0,'Bad Parameter']
    errorcode_parameter_overrange = ['#02',2,'Paramete rOverrange']
    errorcode_pump_in_failure = ['#03',3,'Pump In Failure']
    errorcode_pump_not_in_serial_mode = ['#05',5,'Pump Not In Serial Mode']

    pumpstatus_standby = ['1',1,'Standby']
    pumpstatus_acceleration = ['2',2,'Acceleration']
    pumpstatus_normal = ['3',3,'Normal']
    pumpstatus_brake = ['4',4,'Brake']
    pumpstatus_reacceleration = ['6',6,'ReAcceleration']
    pumpstatus_failure = ['7',7,'Failure']
    pumpstatus_unknown = ['0',0,"Unknown"]

    alarm_none = ['1',1,'No Alarm']
    alarm_change = ['#03',3,'Change Bearing']
    alarm_protection = ['#12',12,'Protection Signal Error']
    alarm_fan_disconnected = ['#20',20,"External Fan isconnected"]
    alarm_system_error = ['#23',23,'System Error']
    alarm_input_voltage_low = ['#30',30,'Input Voltage Low']
    alarm_driver_temperature_error = ['#31',31,'Driver Temperature Error']
    alarm_motor_temperature_error = ['#32',32,'Motor Temperature Error']
    alarm_excessive_current = ['#33',33,'Excessive Current']
    alarm_excessive_rotation_speed = ['#34',34,'Excessive Rotating Speed']
    alarm_acceleration_over_time = ['#35',35,'Acceleration Over Time']
    alarm_PS_Fan_Error = ['#55',55,'PS Fan Error']
    alarm_reacceleration_over_time = ['#60',60,'Reacceleration OverT ime']
    alarm_no_load = ['#61',61,'No Load']

    # Init object
    def __init__(self,serialPort=defaultPortID,baud=defaultBaud,timeout=defaultTimeout):

        self.StartSerial(serialPort,baud,timeout)


    # Open serial object 
    def StartSerial(self,serialPort=defaultPortID,baud=defaultBaud,timeout=defaultTimeout):

        self.pumpSerial = serial.Serial()
        self.pumpSerial.timeout = timeout
        self.pumpSerial.baudrate = baud
        self.pumpSerial.port = serialPort
        self.pumpSerial.open()

        #print self.pumpSerial.name
        return self.pumpSerial

    # Close serial object 
    def StopSerial(self):

        self.pumpSerial.close()


    # response parser

    def ResponseParser(self,response,pumpStatus = False,pumpAlarm = False,pumpSpeed = False):
        try:

            responsestring = response.decode().split()[0]
            


            if responsestring == self.errorcode_bad_parameter[0]:

                return self.errorcode_bad_parameter

            if responsestring == self.errorcode_no_command[0]:

                return self.errorcode_no_command

            if responsestring == self.errorcode_parameter_overrange[0]:

                return self.errorcode_parameter_overrange

            if responsestring == self.errorcode_pump_in_failure[0]:

                return self.errorcode_pump_in_failure

            if responsestring == self.errorcode_pump_not_in_serial_mode[0]:

                return self.errorcode_pump_not_in_serial_mode

            if pumpSpeed:

                return float(responsestring)

            if pumpStatus :
                
                
                if responsestring == self.pumpstatus_acceleration[0]:

                    return self.pumpstatus_acceleration

                elif responsestring == self.pumpstatus_brake[0]:

                    return self.pumpstatus_brake

                elif responsestring == self.pumpstatus_failure[0]:

                    return self.pumpstatus_failure

                elif responsestring == self.pumpstatus_normal[0]:

                    return self.pumpstatus_normal

                elif responsestring == self.pumpstatus_standby[0]:

                    return self.pumpstatus_standby

                elif responsestring == self.pumpstatus_reacceleration[0]:

                    return self.pumpstatus_reacceleration

                else :
                    return self.pumpstatus_unknown


            if pumpAlarm :
                
                
                if responsestring == self.alarm_acceleration_over_time[0]:

                    return self.alarm_acceleration_over_time

                if responsestring == self.alarm_change[0]:

                    return self.alarm_change

                if responsestring == self.alarm_driver_temperature_error[0]:

                    return self.alarm_driver_temperature_error

                if responsestring == self.alarm_excessive_current[0]:

                    return self.alarm_excessive_current

                if responsestring == self.alarm_excessive_rotation_speed[0]:

                    return self.alarm_excessive_rotation_speed

                if responsestring == self.alarm_fan_disconnected[0]:

                    return self.alarm_fan_disconnected

                if responsestring == self.alarm_input_voltage_low[0]:

                    return self.alarm_input_voltage_low

                if responsestring == self.alarm_no_load[0]:

                    return self.alarm_no_load

                if responsestring == self.alarm_protection[0]:

                    return self.alarm_protection

                if responsestring == self.alarm_PS_Fan_Error[0]:

                    return self.alarm_PS_Fan_Error

                if responsestring == self.alarm_reacceleration_over_time[0]:

                    return self.alarm_reacceleration_over_time

                if responsestring == self.alarm_motor_temperature_error[0]:

                    return self.alarm_motor_temperature_error

                if responsestring == '1':

                    return self.alarm_none

            else:
                
                return self.pumpstatus_unknown

        except:

            print(str(datetime.now()))
            traceback.print_exc()

    # CRC Command

    def DisableCRC(self):

        self.pumpSerial.write(str('SCC0b89a'+self.terminator).encode())
        response = self.pumpSerial.read_until(self.terminator)

        return self.ResponseParser(response)

    def WriteSerial(self,value):

        # writes a serial command and reads from the receive buffer. does not decode. if timeout,, excepts, closes the serial port, reopens the serial port, and tries again.
        while(True):
            try:
                self.pumpSerial.write(str(str(value)+self.terminator).encode())
                response = self.pumpSerial.read_until(self.terminator)

                return response, True

            except:
                print(str(datetime.now())+': timeout. Restarting comms....')
                self.pumpSerial.close()
                self.pumpSerial.open()
    

    # Start Pump

    def StartPump(self):

        response, readback = self.WriteSerial('SDR1')

        return self.ResponseParser(response)

    # Stop Pump

    def StopPump(self):

        response, readback = self.WriteSerial('SDR0')
        

        return self.ResponseParser(response)

    # Pump operational time

    def OperationalTime(self):

        response, readback = self.WriteSerial('RDT')
        

        return self.ResponseParser(response)

    # Read Pump Status

    def PumpStatus(self):
        try:
            response, readback = self.WriteSerial('RSS')            

            return self.ResponseParser(response,pumpStatus=True)

        except:
            print("something be hosed with pumpstatus.")
            traceback.print_exc()

    # read alarm details

    def PumpAlarm(self):

        response, readback = self.WriteSerial('RSA')

        return self.ResponseParser(response,pumpAlarm=True)

    # read speed

    def PumpSpeed(self):

        response, readback = self.WriteSerial('RRS')
        

        return self.ResponseParser(response,pumpSpeed=True)
